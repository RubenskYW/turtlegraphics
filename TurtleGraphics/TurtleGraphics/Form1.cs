﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
//using System.Collections;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurtleGraphics
{
    public partial class Form1 : Form
    {
        ArrayList shapes = new ArrayList();
        public Form1()
        {
            InitializeComponent();

            ShapeFactory factory = new ShapeFactory();
            try
            {
                shapes.Add(factory.getShape("circle"));
                shapes.Add(factory.getShape("triangle"));
                shapes.Add(factory.getShape("rectangle"));
            }
            catch (ArgumentException en)
            {
                Console.WriteLine("Invalid" + en);
            }

            

        }

        public void draw_Click(object sender, EventArgs e)
        {
            Turtle.Delay = 200;

            // Draw a equilateral triangle
            Turtle.Rotate(30);
            Turtle.Forward(200);
            Turtle.Rotate(120);
            Turtle.Forward(200);
            Turtle.Rotate(120);
            Turtle.Forward(200);

            // Draw a line in the triangle
            Turtle.Rotate(-30);
            Turtle.PenUp();
            Turtle.Backward(50);
            Turtle.PenDown();
            Turtle.Backward(100);
            Turtle.PenUp();
            Turtle.Forward(150);
            Turtle.PenDown();
            Turtle.Rotate(30);
        }
        private const int ComparisonCount = 500 * 100;
        private void cmdBox_TextChanged(object sender, EventArgs e)
        {
            
            String cmd = cmdBox.Text;
            for (int i = 0; i < ComparisonCount; i++)
            {
                if (String.Equals(cmd,cmd,
               StringComparison.OrdinalIgnoreCase))
                {
                    DoNothing();
                }
            }
            if (cmd == "penDown")
            {
                Turtle.PenDown();
            }
            if (cmd == "penUp")
            {
                Turtle.PenUp();
            }
            if (cmd == "forward")
            {
                Turtle.Forward(100);
            }
            if (cmd == "backward")
            {
                Turtle.Backward(100);
            }
            if (cmd == "right")
            {
                Turtle.Rotate(90);
            }
            if (cmd == "left")
            {
                Turtle.Rotate(-90);
            }
            if (cmd == "reset")
            {
                Turtle.MoveTo(0, 0);
            }
            if (cmd == "clear")
            {
                Turtle.Dispose();
            }
            if (cmd == "elip")
            {
                Turtle.Ellipse();
            }
            if (cmd == "triangle")
            {
                Turtle.Rotate(30);
                Turtle.Forward(200);
                Turtle.Rotate(120);
                Turtle.Forward(200);
                Turtle.Rotate(120);
                Turtle.Forward(200);
            }
            if (cmd == "moveto")
            {
                Turtle.MoveTo(-250 ,250);
            }
            if (cmd == "Square")
            {
                
            }
          
        }

        private void DoNothing()
        {
            
        }

        private void SAVE_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                    (System.IO.FileStream)saveFileDialog1.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        this.SAVE.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        this.SAVE.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        this.SAVE.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }

            }
        }

        private void setBkgrd_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK) // if statment with ShowDialog() method
                // pick a color using "colorDialog"
                pictureBox1.BackColor = colorDialog1.Color;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Bitmap bitmap;
        }

        private void loadArea_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)//if returns OK if user clicks okay
            {
              pictureBox1.Load(openFileDialog1.FileName);//load the file chosen
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
