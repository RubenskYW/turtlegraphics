﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TurtleGraphics
{
    abstract class Shape:Shapes 
    {
        protected Color colour;
        protected int x, y;


        public Shape()
        {
            colour = Color.Green;
            x = y = 100;
        }

        public Shape(Color colour, int x, int y)
        {
            this.colour = colour; //shape's colour
            this.x = x; //its x pos
            this.y = y;
        }

        public abstract void draw(Graphics graphics);

        public abstract double calcArea();

        public abstract double calPerimeter();

        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            this.x = list[0];
            this.y = list[1];
        }
    }
}
