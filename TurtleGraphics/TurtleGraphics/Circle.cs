﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TurtleGraphics
{
    class Circle : Shape
    {
        int radius;
        public Circle(Color color, int x, int y, int radius) : base(color, x, y)
        {
            this.radius = radius;
        }

        public Circle():base()
        {
        }

        public override void set(Color color, params int[] list)
        {
            //list[0] is x, list[1] is y, list[2] is radius
            base.set(color, list[0], list[1]);
            this.radius = list[2];


        }

        public override void draw(Graphics graphics)
        {

            Pen p = new Pen(Color.Black, 2);
            SolidBrush b = new SolidBrush(colour);
            graphics.FillEllipse(b, x, y, radius * 2, radius * 2);
            graphics.DrawEllipse(p, x, y, radius * 2, radius * 2);

        }

        public override double calcArea()
        {
            return Math.PI * (radius ^ 2);
        }

        public override double calPerimeter()
        {
            return 2 * Math.PI * radius;
        }

        public override string ToString() //all classes inherit from object and ToString() is abstract in object
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
