﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TurtleGraphics
{
    class Square:Rectangle
    {
        private int size;

        public Square() : base()
        {

        }
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        
        public override void draw(Graphics g)
        {
            base.draw(g);
        }

    }
}
